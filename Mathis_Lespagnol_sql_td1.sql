
------------------------------------------------------------------ 
-- select
------------------------------------------------------------------ 

select * from patient;
-- Affiche la table des patients 

select id_patient, sexe from patient;
-- selectionne et affiche les colonnes des identifiants patients et des sexes des patients


select count(*) from patient;
-- compte le nombre de lignes dans le fichier patient : on a le nombre de patients


-- Q1 Afficher la table séjour 
select * from sejour ;


-- where
------------------------------------------------------------------ 

select *
from patient 
where sexe = 'M';
-- selectionne les patient de sexe masculin

select *
from patient
where date_naissance > '1960-01-01';
-- selectionne les patients nés après le 01 janvier 1960


-- Q2 Sélectionner les patients de la ville 1
select *
from patient
where id_ville = '1'



-- Q3 Afficher les patients nés après le 31/03/1986
select *
from patient
where date_naissance > '1986-03-31';

-- AND
------------------------------------------------------------------ 

select *
from patient
where date_naissance > '1960-01-01'
and sexe = 'F';

-- affiche les patients nés après le 01 janvier 1960 et de sexe féminin

-- Q4 Afficher les séjours commencés après le 01/02/2020 dans l’hôpital 1
select *
from sejour
where id_hopital = '1'
and date_debut_sejour > '2020-02-01'

-- IN
------------------------------------------------------------------ 

select * 
from patient
where id_ville in (1, 2);
-- selectionne les patient de Lille ou de Roubaix

-- Q5 Afficher les séjours des hôpitaux 1 et 3
select * 
from sejour
where id_hopital in (1, 3);

-- GROUP BY
------------------------------------------------------------------ 

select sexe, count(*)
from patient
group by sexe;
-- compte le nombre de patients selon leur sexe

-- Q6 Compter le nombre de patient par ville

select id_ville, count(*) as nb_patient
from patient
group by id_ville;

-- Q7 Compter le nombre de séjours par hopital
select id_hopital, count(*) as nb_sejour
from sejour
group by id_hopital



-- INNER JOIN
------------------------------------------------------------------ 

select *
from patient p inner join ville v 
on p.id_patient = v.id_ville;
-- la requete n'est pas correcte car on effectue une jointure sur des clés différentes

-- Q8 Modifier la requête précédente pour n'afficher que l'id_patient et le nom de la ville
select p.id_patient, v.ville
from patient p inner join ville v 
on p.id_ville = v.id_ville;

-- Q9 Afficher, pour chaque séjour, les hôpitaux dans lesquels ils ont eu lieu
select id_sejour, s.id_hopital
from sejour s inner join hopital h
on s.id_hopital = h.id_hopital ;


-- Q10 Compter le nombre de patients par ville en affichant le NOM de la ville
select v.ville, count(*)  as nb_patient
from patient p inner join ville v
on p.id_ville = v.id_ville 
group by v.ville


-- Q11 Compter le nombre de séjours par hôpital en affichant le NOM de l'hôpital
select h.hopital , count(*) as nb_sejour
from sejour s inner join hopital h
on s.id_hopital= h.id_hopital
group by h.hopital

-- Q12 Compter le nombre de patients femme par ville en affichant le nom de la ville
select v.ville, count (*) as nb_patiente
from patient p inner join ville v
on p.id_ville= v.id_ville
where p.sexe= 'F'
group by v.ville


-- Q13 Compter le nombre de séjours commençés après le 01/02/2020 pour chaque hôpital en affichant le nom de l'hôpital
select h.hopital, count(*) as nb_sejour
from sejour s inner join hopital h
on s.id_hopital = h.id_hopital 
where s.date_debut_sejour > '2020-02-01'
group by h.hopital ;


-- insert
------------------------------------------------------------------ 

-- Exécuter la requête et **interpréter** le résultat :

INSERT INTO ville
(id_ville, ville)
VALUES(6, 'Béthune');
-- insert une ligne dans la table ville avec la ville de Béthune et son identifiant à 6

-- Q13 Insérer Loos dans la table ville
INSERT INTO ville
(id_ville, ville)
VALUES(7, 'Loos');

-- update
------------------------------------------------------------------ 

-- Exécuter la requête et **interpréter** le résultat :

update ville set ville = 'Lens' where id_ville = 6;
-- Mets à jour la ligne de la table Ville avec l'identifiant 6 avec le nom Lens au lieu de Béthune

-- Q14 Remplacer le libellé de la ville numéro 7 par Douai
update ville set ville = 'Douai' where id_ville = 7;


-- delete
------------------------------------------------------------------ 

-- Exécuter la requête et **interpréter** le résultat :

delete from ville where id_ville = 6;
-- supprime la ligne de Lens dans la table Ville

-- Q15 supprimer la ville numéro 7
delete from ville where id_ville = 7;

